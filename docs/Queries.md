

```sql

SELECT
       symbol,
       close_run_inactivity_stop,
       close_run_length,
       COUNT(*),
       ROUND(AVG(close_run_change), 4)
FROM symbol_bars
WHERE close_run_change > 1
GROUP BY symbol, close_run_inactivity_stop, close_run_length
ORDER BY symbol, close_run_inactivity_stop, close_run_length desc;

SELECT sb.symbol, DATE_FORMAT(measured_at, "%H:%i") AS time, COUNT(*) AS count, AVG(high/low) AS percent
FROM symbol_bars sb
JOIN (
    SELECT symbol, ROUND(AVG(volume), 0) AS average, ROUND(STD(volume), 0) as deviation
    FROM symbol_bars
    GROUP BY symbol
    ) sba ON sba.symbol = sb.symbol
WHERE sb.volume > sba.average + 2 * sba.deviation AND sb.symbol="TSLA"
GROUP BY sb.symbol, DATE_FORMAT(measured_at, "%H:%i")
ORDER BY percent DESC;


-- High Volume Bars
SELECT *
FROM symbol_bars sb
JOIN (
    SELECT symbol, ROUND(AVG(volume), 0) AS average, ROUND(STD(volume), 0) as deviation
    FROM symbol_bars
    GROUP BY symbol
    ) sba ON sba.symbol = sb.symbol
WHERE sb.volume > sba.average + 2 * sba.deviation;




-- High Movement Periods
SELECT symbol, ROUND(AVG(volume), 0) AS average, ROUND(STD(volume), 0) as deviation
FROM symbol_bars
GROUP BY symbol;

-- Correlations w\PE
SELECT
       DATE_PART('year', measured_at),
       round(pe_ratio/5, 0),
       CORR(pe_ratio, max_gain) AS max_corr
FROM (
       SELECT d.measured_at,
              r.symbol,
              d.adjusted_close,
              r.reported_eps,
              d.adjusted_close / r.reported_eps AS pe_ratio,
              q.max_gain,
              q.min_gain
       FROM quarterly_earnings_reports r
              JOIN days d ON d.symbol = r.symbol AND d.measured_at = r.fiscal_date_ending + INTERVAL '2 day'
              JOIN day_futures_quarter q
                   ON q.symbol = r.symbol AND q.measured_at = r.fiscal_date_ending + INTERVAL '2 day'
       WHERE r.reported_eps <> 0 AND d.adjusted_close > 0 and d.adjusted_close IS NOT NULL

     ) eps
GROUP BY DATE_PART('year', measured_at), round(pe_ratio/5, 0)
having count(*) > 50

```


