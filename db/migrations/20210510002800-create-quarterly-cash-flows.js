'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('quarterly_cash_flows', {
      symbol: {
        type: Sequelize.STRING(20),
        primaryKey: true
      },
      fiscal_date_ending: {
        type: Sequelize.DATEONLY,
        primaryKey: true
      },
      reported_currency: {
        type: Sequelize.STRING(5)
      },
      operating_cashflow: Sequelize.DECIMAL(20, 2),
      payments_for_operating_activities: Sequelize.DECIMAL(20, 2),
      proceeds_from_operating_activities: Sequelize.DECIMAL(20, 2),
      change_in_operating_liabilities: Sequelize.DECIMAL(20, 2),
      change_in_operating_assets: Sequelize.DECIMAL(20, 2),
      depreciation_depletion_and_amortization: Sequelize.DECIMAL(20, 2),
      capital_expenditures: Sequelize.DECIMAL(20, 2),
      change_in_receivables: Sequelize.DECIMAL(20, 2),
      change_in_inventory: Sequelize.DECIMAL(20, 2),
      profit_loss: Sequelize.DECIMAL(20, 2),
      cashflow_from_investment: Sequelize.DECIMAL(20, 2),
      cashflow_from_financing: Sequelize.DECIMAL(20, 2),
      proceeds_from_repayments_of_short_term_debt: Sequelize.DECIMAL(20, 2),
      payments_for_repurchase_of_common_stock: Sequelize.DECIMAL(20, 2),
      payments_for_repurchase_of_equity: Sequelize.DECIMAL(20, 2),
      payments_for_repurchase_of_preferred_stock: Sequelize.DECIMAL(20, 2),
      dividend_payout: Sequelize.DECIMAL(20, 2),
      dividend_payout_common_stock: Sequelize.DECIMAL(20, 2),
      dividend_payout_preferred_stock: Sequelize.DECIMAL(20, 2),
      proceeds_from_issuance_of_common_stock: Sequelize.DECIMAL(20, 2),
      proceeds_from_issuance_of_lt_debt_and_cap_securities_net: Sequelize.DECIMAL(20, 2),
      proceeds_from_issuance_of_preferred_stock: Sequelize.DECIMAL(20, 2),
      proceeds_from_repurchase_of_equity: Sequelize.DECIMAL(20, 2),
      proceeds_from_sale_of_treasury_stock: Sequelize.DECIMAL(20, 2),
      change_in_cash_and_cash_equivalents: Sequelize.DECIMAL(20, 2),
      change_in_exchange_rate: Sequelize.DECIMAL(20, 2),
      net_income: Sequelize.DECIMAL(20, 2),
      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.fn('now')
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.fn('now')
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('quarterly_cash_flows');
  }
};
