'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('days', {
      symbol: {
        type: Sequelize.STRING(20),
        primaryKey: true
      },
      measured_at: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.DATEONLY
      },
      open: {
        type: Sequelize.DECIMAL(20, 2)
      },
      high: {
        type: Sequelize.DECIMAL(20, 2)
      },
      low: {
        type: Sequelize.DECIMAL(20, 2)
      },
      close: {
        type: Sequelize.DECIMAL(20, 2)
      },
      volume: {
        type: Sequelize.BIGINT
      },
      adjusted_close: {
        type: Sequelize.DECIMAL(20, 2)
      },
      dividend: {
        type: Sequelize.DECIMAL(20, 2)
      },
      split: {
        type: Sequelize.FLOAT
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.fn('now')
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.fn('now')
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('days');
  }
};