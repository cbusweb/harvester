'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('bars', {
      symbol: {
        type: Sequelize.STRING(20),
        primaryKey: true
      },
      measured_at: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.DATE
      },
      open: {
        type: Sequelize.DECIMAL(20, 2)
      },
      high: {
        type: Sequelize.DECIMAL(20, 2)
      },
      low: {
        type: Sequelize.DECIMAL(20, 2)
      },
      close: {
        type: Sequelize.DECIMAL(20, 2)
      },
      volume: {
        type: Sequelize.INTEGER
      },
      close_run_length: {
        type: Sequelize.INTEGER
      },
      close_run_change: {
        type: Sequelize.DECIMAL(20, 2)
      },
      close_run_volume: {
        type: Sequelize.DECIMAL(20, 2)
      },
      close_run_inactivity_stop: {
        type: Sequelize.BOOLEAN
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('bars');
  }
};