'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('quarterly_earnings_reports', {
      symbol: {
        type: Sequelize.STRING(20),
        primaryKey: true
      },
      fiscal_date_ending: {
        type: Sequelize.DATEONLY,
        primaryKey: true
      },
      reported_date: {
        type: Sequelize.DATEONLY
      },
      reported_eps: {
        type: Sequelize.DECIMAL(20, 2)
      },
      estimated_eps: {
        type: Sequelize.DECIMAL(20, 2)
      },
      surprise: {
        type: Sequelize.DECIMAL(20, 2)
      },
      surprise_percentage: {
        type: Sequelize.DECIMAL(20, 2)
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.fn('now')
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.fn('now')
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('quarterly_earnings_reports');
  }
};