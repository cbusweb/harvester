'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('dividends', {
      symbol: {
        type: Sequelize.STRING(20),
        primaryKey: true
      },
      executed_date: {
        type: Sequelize.DATEONLY,
        primaryKey: true
      },
      record_date: {
        type: Sequelize.DATEONLY
      },
      payment_date: {
        type: Sequelize.DATEONLY
      },
      amount: {
        type: Sequelize.DECIMAL(20, 2)
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('dividends');
  }
};