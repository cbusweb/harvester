'use strict';
const fs = require('fs');

module.exports = {
  up: async (queryInterface, Sequelize) => {
    let sql = fs.readFileSync(__dirname + '/views/20210510003000-create-day-futures.sql', 'utf8');
    await queryInterface.sequelize.query(sql.replace('{DAYS}', '7').replace('{PERIOD}', 'week'));
    await queryInterface.sequelize.query(sql.replace('{DAYS}', '28').replace('{PERIOD}', 'month'));
    await queryInterface.sequelize.query(sql.replace('{DAYS}', '91').replace('{PERIOD}', 'quarter'));
    await queryInterface.sequelize.query(sql.replace('{DAYS}', '182').replace('{PERIOD}', 'semester'));
    await queryInterface.sequelize.query(sql.replace('{DAYS}', '364').replace('{PERIOD}', 'year'));

  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.sequelize.query("DROP VIEW IF EXISTS `day_futures_week`;");
    await queryInterface.sequelize.query("DROP VIEW IF EXISTS `day_futures_month`;");
    await queryInterface.sequelize.query("DROP VIEW IF EXISTS `day_futures_quarter`;");
    await queryInterface.sequelize.query("DROP VIEW IF EXISTS `day_futures_semester`;");
    await queryInterface.sequelize.query("DROP VIEW IF EXISTS `day_futures_year`;");
  }
};
