'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('quarterly_balance_sheets', {
      symbol: {
        type: Sequelize.STRING(20),
        primaryKey: true
      },
      fiscal_date_ending: {
        type: Sequelize.DATEONLY,
        primaryKey: true
      },
      reported_currency: {
        type: Sequelize.STRING(5)
      },
      total_assets: {
        type: Sequelize.DECIMAL(20, 2)
      },
      total_current_assets: {
        type: Sequelize.DECIMAL(20, 2)
      },
      cash_and_cash_equivalents_at_carrying_value: {
        type: Sequelize.DECIMAL(20, 2)
      },
      cash_and_short_term_investments: {
        type: Sequelize.DECIMAL(20, 2)
      },
      inventory: {
        type: Sequelize.DECIMAL(20, 2)
      },
      current_net_receivables: {
        type: Sequelize.DECIMAL(20, 2)
      },
      total_non_current_assets: {
        type: Sequelize.DECIMAL(20, 2)
      },
      property_plant_equipment: {
        type: Sequelize.DECIMAL(20, 2)
      },
      accumulated_depreciation_amortization_ppe: {
        type: Sequelize.DECIMAL(20, 2)
      },
      intangible_assets: {
        type: Sequelize.DECIMAL(20, 2)
      },
      intangible_assets_excluding_goodwill: {
        type: Sequelize.DECIMAL(20, 2)
      },
      goodwill: {
        type: Sequelize.DECIMAL(20, 2)
      },
      investments: {
        type: Sequelize.DECIMAL(20, 2)
      },
      long_term_investments: {
        type: Sequelize.DECIMAL(20, 2)
      },
      short_term_investments: {
        type: Sequelize.DECIMAL(20, 2)
      },
      other_current_assets: {
        type: Sequelize.DECIMAL(20, 2)
      },
      other_non_currrent_assets: {
        type: Sequelize.DECIMAL(20, 2)
      },
      total_liabilities: {
        type: Sequelize.DECIMAL(20, 2)
      },
      total_current_liabilities: {
        type: Sequelize.DECIMAL(20, 2)
      },
      current_accounts_payable: {
        type: Sequelize.DECIMAL(20, 2)
      },
      deferred_revenue: {
        type: Sequelize.DECIMAL(20, 2)
      },
      current_debt: {
        type: Sequelize.DECIMAL(20, 2)
      },
      short_term_debt: {
        type: Sequelize.DECIMAL(20, 2)
      },
      total_non_current_liabilities: {
        type: Sequelize.DECIMAL(20, 2)
      },
      capital_lease_obligations: {
        type: Sequelize.DECIMAL(20, 2)
      },
      long_term_debt: {
        type: Sequelize.DECIMAL(20, 2)
      },
      current_long_term_debt: {
        type: Sequelize.DECIMAL(20, 2)
      },
      long_term_debt_non_current: {
        type: Sequelize.DECIMAL(20, 2)
      },
      short_long_term_debt_total: {
        type: Sequelize.DECIMAL(20, 2)
      },
      other_current_liabilities: {
        type: Sequelize.DECIMAL(20, 2)
      },
      other_non_current_liabilities: {
        type: Sequelize.DECIMAL(20, 2)
      },
      total_shareholder_equity: {
        type: Sequelize.DECIMAL(20, 2)
      },
      treasury_stock: {
        type: Sequelize.DECIMAL(20, 2)
      },
      retained_earnings: {
        type: Sequelize.DECIMAL(20, 2)
      },
      common_stock: {
        type: Sequelize.DECIMAL(20, 2)
      },
      common_stock_shares_outstanding: {
        type: Sequelize.DECIMAL(20, 2)
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.fn('now')
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.fn('now')
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('quarterly_balance_sheets');
  }
};
