'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('quarterly_income_statements', {
      symbol: {
        type: Sequelize.STRING(20),
        primaryKey: true
      },
      fiscal_date_ending: {
        type: Sequelize.DATEONLY,
        primaryKey: true
      },
      reported_currency: {
        type: Sequelize.STRING(5)
      },
      gross_profit: {
        type: Sequelize.DECIMAL(20, 2)
      },
      total_revenue: {
        type: Sequelize.DECIMAL(20, 2)
      },
      cost_of_revenue: {
        type: Sequelize.DECIMAL(20, 2)
      },
      cost_of_goods_and_services_sold: {
        type: Sequelize.DECIMAL(20, 2)
      },
      operating_income: {
        type: Sequelize.DECIMAL(20, 2)
      },
      selling_general_and_administrative: {
        type: Sequelize.DECIMAL(20, 2)
      },
      research_and_development: {
        type: Sequelize.DECIMAL(20, 2)
      },
      operating_expenses: {
        type: Sequelize.DECIMAL(20, 2)
      },
      investment_income_net: {
        type: Sequelize.DECIMAL(20, 2)
      },
      net_interest_income: {
        type: Sequelize.DECIMAL(20, 2)
      },
      interest_income: {
        type: Sequelize.DECIMAL(20, 2)
      },
      interest_expense: {
        type: Sequelize.DECIMAL(20, 2)
      },
      non_interest_income: {
        type: Sequelize.DECIMAL(20, 2)
      },
      other_non_operating_income: {
        type: Sequelize.DECIMAL(20, 2)
      },
      depreciation: {
        type: Sequelize.DECIMAL(20, 2)
      },
      depreciation_and_amortization: {
        type: Sequelize.DECIMAL(20, 2)
      },
      income_before_tax: {
        type: Sequelize.DECIMAL(20, 2)
      },
      income_tax_expense: {
        type: Sequelize.DECIMAL(20, 2)
      },
      interest_and_debt_expense: {
        type: Sequelize.DECIMAL(20, 2)
      },
      net_income_from_continuing_operations: {
        type: Sequelize.DECIMAL(20, 2)
      },
      comprehensive_income_net_of_tax: {
        type: Sequelize.DECIMAL(20, 2)
      },
      ebit: {
        type: Sequelize.DECIMAL(20, 2)
      },
      ebitda: {
        type: Sequelize.DECIMAL(20, 2)
      },
      net_income: {
        type: Sequelize.DECIMAL(20, 2)
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.fn('now')
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.fn('now')
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('quarterly_income_statements');
  }
};
