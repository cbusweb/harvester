CREATE OR REPLACE VIEW day_futures_{PERIOD} (symbol, measured_at, min_gain, max_gain)
AS SELECT
       current.symbol,
       current.measured_at,
       MIN((future.adjusted_close - current.adjusted_close) / current.adjusted_close) AS min_gain,
       MAX((future.adjusted_close - current.adjusted_close) / current.adjusted_close) AS max_gain
   FROM
       days current
           JOIN days future
                ON current.symbol = future.symbol
                    AND current.measured_at < future.measured_at
                    AND future.measured_at < current.measured_at + (({DAYS} + 1) * INTERVAL '1 day')
   WHERE current.adjusted_close > 0
   GROUP BY current.symbol, current.measured_at