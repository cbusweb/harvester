'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('securities', {
      symbol: {
        type: Sequelize.STRING(20),
        primaryKey: true
      },
      name: {
        type: Sequelize.STRING(255),
        allowNull: false
      },
      exchange: {
        type: Sequelize.STRING(10),
        allowNull: false
      },
      type: {
        type: Sequelize.STRING(10),
        allowNull: false
      },
      ipo_date: {
        type: Sequelize.DATEONLY
      },
      delisting_date: {
        type: Sequelize.DATEONLY
      },
      status: {
        type: Sequelize.STRING(10),
        allowNull: false
      },
      last_daily_poll: {
        allowNull: true,
        type: Sequelize.DATE,
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.fn('now')
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.fn('now')
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('securities');
  }
};