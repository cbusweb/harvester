# Exit on any error.
set -e

# Recreate database.
# cross-env NODE_ENV=default npx sequelize-cli db:create
cross-env NODE_ENV=default npx sequelize-cli db:drop
cross-env NODE_ENV=default npx sequelize-cli db:create

# Migrate and seed.
cross-env NODE_ENV=default npx sequelize-cli db:migrate
cross-env NODE_ENV=default npx sequelize-cli db:seed:all
