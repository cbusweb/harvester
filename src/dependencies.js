// Import NPM dependencies.
import Bottle from 'bottlejs';
import dotenv from 'dotenv';
import { EventBridgeClient } from "@aws-sdk/client-eventbridge";
import { restClient } from "@polygon.io/client-js";
import S3 from 'aws-sdk/clients/s3';

// Internal libraries.
import AlphaVantageManager from './services/alpha_vantage_manager';
import CacheManager from './services/cache_manager';
import PolygonManager from './services/polygon_manager';
import EventBridgeManager from "./services/eventbridge/manager";

// Add various dependencies.
const Alpaca = require('@alpacahq/alpaca-trade-api');
const AWS = require('aws-sdk');
const winston = require('winston');

// Initialize environment.
dotenv.config();

// Create DI container.
const bottle = new Bottle();

bottle.factory('Logger', () => {
    return winston.createLogger({
        level: process.env.LOG_LEVEL || 'info',
        throwHttpErrors: false,
        format: winston.format.combine(
            winston.format.json(),
            winston.format.errors({ stack: true })
        ),
        transports: [
            new winston.transports.Console({
                level: process.env.LOG_LEVEL ? process.env.LOG_LEVEL : 'info'
            }),
        ]
    });
});

bottle.factory('S3Client', () => {
    return new S3({
        s3ForcePathStyle: true,
        accessKeyId: 'S3RVER', // This specific key is required when working offline
        secretAccessKey: 'S3RVER',
        endpoint: new AWS.Endpoint('http://localhost:4569'),
    });
});

bottle.factory('CacheManager', (container) => {
    return new CacheManager({
        bucket: 'cache-bucket',
        client: container.S3Client,
        logger: container.Logger,
    });
});

/*
bottle.factory('CacheManager', () => {
    const cacheManager = require('cache-manager');
    const S3Cache = require('cache-manager-s3');
    const store = new S3Cache({
        accessKey: 'S3RVER',
        secretKey: 'S3RVER',
        bucket: 'cache-bucket',
        ttl: 3600*24*365*30,
        parseKeyAsPath: true,
        normalizePath: true,
        normalizeLowercase: true,
        s3Options: {
            endpoint: new AWS.Endpoint('http://localhost:4569'),
            s3ForcePathStyle: true,
        },
    });
    return cacheManager.caching({
        store: store,
    });
});
*/

// A standard AWS EventBridge Client.
bottle.factory('EventBridgeClient', function(container) {
    // TODO: This will need to be adjusted for live deploy.
    return new EventBridgeClient ({
        endpoint: 'http://127.0.0.1:4010',
        region: "us-east-1",
        debug: process.env.LOG_LEVEL == 'debug'
    });
});

// The manager is a wrapper around the standard event bridge client so we
// can simplify things like message creation, submission, logging, etc.
bottle.factory('EventBridgeManager', function(container) {
    return new EventBridgeManager({
        bus: process.env.EVENT_BUS,
        client: container.EventBridgeClient,
        logger: container.Logger,
        source: 'Harvester',
    });
});

bottle.factory('AlphaVantageClient',  (container) => {
    return require('alphavantage')({ key: process.env.ALPHAVANTAGE_API_KEY });
});

bottle.factory('AlphaVantageManager',  (container) => {
    return new AlphaVantageManager({
        alphaVantageClient: container.AlphaVantageClient,
        apiKey: process.env.ALPHAVANTAGE_API_KEY,
        logger: container.Logger,
    })
});

bottle.factory('AlpacaClient',  (container) => {
    return new Alpaca({
        keyId: process.env.ALPACA_API_KEY_ID,
        secretKey: process.env.ALPACA_SECRET_KEY,
        paper: false,
        usePolygon: false
    })
});

bottle.factory('PolygonClient', (container) => {
    return restClient(process.env.POLYGON_API_KEY);
});

bottle.factory('PolygonManager', (container) => {
    return new PolygonManager({
        polygonClient: container.PolygonClient,
        logger: container.Logger,
    });
});

bottle.factory('TwitterClient', function(container) {
    const Twitter = require('twitter-v2');
    return new Twitter({
        bearer_token: process.env.TWITTER_BEARER_TOKEN
    });
});


export default bottle.container;
