export default class PolygoneManager {

    client;

    /** @type {Logger} */
    logger;

    /**
     * Creates an EventBridgeManager
     * @param options
     */
    constructor(options) {
        this.client = options.polygonClient;
        this.logger = options.logger;
    }

    async dividends(symbol) {
        return this.client.reference.stockDividends(symbol);
    }
}
