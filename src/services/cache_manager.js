

export default class CacheManager {
    client;

    /** @type {Logger} */
    logger;

    /**
     * Creates an EventBridgeManager
     * @param options
     */
    constructor(options) {
        this.bucket = 'cache-bucket';
        this.client = options.client;
        this.logger = options.logger;
    }

    async get(key) {
        try {
            const result = await this.client.getObject({
                Bucket: this.bucket,
                Key: key
            }).promise();
            const parsed = JSON.parse(result.Body);
            return parsed ? parsed : null;
        }
        catch (exception) {
            if (exception.message == 'The specified key does not exist.') {
                this.logger.info('Cache miss for ' + key);
            }
            else {
                throw exception;
            }
        }
    }

    async set(key, data) {
        await this.client.putObject({
            Bucket: this.bucket,
            Key: key,
            Body: new Buffer.from(JSON.stringify(data, null, 2))
        }).promise();
    }
}
