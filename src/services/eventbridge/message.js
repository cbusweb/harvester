export default class EventBridgeMessage {

    /** @type {String} */
    bus;

    /** @type {Array} */
    entries;

    /** @type {Logger} */
    logger;

    /** @type {String} */
    source;

    /**
     * Creates an EventBridgeMessage.
     * @param options
     */
    constructor(options) {
        this.bus = options.bus;
        this.logger = options.logger;
        this.source = options.source;
        this.entries = [];
    }

    addEntry(detailType, detail={}) {
        this.entries.push({
            EventBusName: this.bus,
            Source: this.source,
            DetailType: detailType,
            Detail: JSON.stringify(detail),
        });
    }

    getEntries() {
        return this.entries;
    }

}
