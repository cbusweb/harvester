import { EventBridgeClient, PutEventsCommand } from "@aws-sdk/client-eventbridge";
import EventBridgeMessage from "./message";

export default class EventBridgeManager {

    /**
     * Creates an EventBridgeManager
     * @param options
     */
    constructor(options) {
        /**
         * EventBridge client.
         * @type {EventBridgeClient}
         */
        this.client = options.client;
        this.logger = options.logger;
        this.bus = options.bus;
        this.source = options.source;
        this.mockEvents = options.mockEvents;
    }

    createMessage() {
        return new EventBridgeMessage({
            bus: this.bus,
            source: this.source,
            logger: this.logger,
        });
    }

    /**
     * Sends a message
     * @param {EventBridgeMessage} message
     * @returns {Promise<unknown>}
     */
    async send(message) {
        try {
            const results = [];
            const messages = message.getEntries();

            // One can only send 10 events at a time so if there are many entries, we break them up.
            while (messages.length > 0) {
                let command = new PutEventsCommand({
                    Entries: messages.splice(0, 10)
                });
                this.logger.debug('Sending PutEventsCommand to EventBridge', command);
                let result = await this.client.send(command);
                this.logger.debug('Results of PutEventsCommand', result);
                results.push(result);
            }
            return results;
        }
        catch (e) {
            console.error(e);
            return { statusCode: 400, body: 'could not publish' };
        }
        console.log("I'm done bro.");
    }
}
