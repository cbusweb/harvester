const fetch = require('node-fetch');
const parse = require('csv-parse/lib/sync')
export default class AlphaVantageManager {
    apiKey;

    client;

    /** @type {Logger} */
    logger;

    /**
     * Creates an EventBridgeManager
     * @param options
     */
    constructor(options) {
        this.apiKey = options.apiKey;
        this.client = options.alphaVantageClient;
        this.logger = options.logger;
    }

    async days(symbol) {
        let url = `https://www.alphavantage.co/query?function=TIME_SERIES_DAILY_ADJUSTED&symbol=${symbol}&outputsize=full&datatype=csv&apikey=${this.apiKey}`
        const response = await fetch(url);
        const data = await response.text();
        const records = parse(data, {
            columns: true,
            cast: true,
            skip_empty_lines: true
        })
        return records;
    }

    async listings() {
        const states = ['active', 'delisted'];
        let data = '';
        for (let i in states) {
            let url = `https://www.alphavantage.co/query?function=LISTING_STATUS&state=${states[i]}&apikey=${this.apiKey}`;
            let response = await fetch(url);
            if (data.length > 0) {
                let nextData = await response.text();
                data += nextData.substring(nextData.indexOf("\n") + 1);
            }
            else {
                data = await response.text();
            }
        }

        const records = parse(data, {
            columns: true,
            cast: true,
            skip_empty_lines: true
        });
        return records;
    }

    async balance(symbol, cachePrefix='active') {
        return this.getSymbolData('BALANCE_SHEET', symbol);
    }

    async cashFlow(symbol, cachePrefix='active') {
        return this.getSymbolData('CASH_FLOW', symbol);
    }

    async earnings(symbol, cachePrefix='active') {
        return this.getSymbolData('EARNINGS', symbol);
    }

    async income(symbol, cachePrefix='active') {
        return this.getSymbolData('INCOME_STATEMENT', symbol);
    }

    async intraday(symbol, interval='60min', period=null, cachePrefix='active') {
        return this.client.data.intraday(symbol, symbol, '60min');
    }

    async getSymbolData(fn, symbol, options={}) {
        let url = `https://www.alphavantage.co/query?function=${fn}&symbol=${symbol}&apikey=${this.apiKey}`
        for (const property in options) {
            url += '&' + encodeURIComponent(property) + '=' + encodeURIComponent(options[property]);
        }
        const response = await fetch(url, {});
        const data = await response.json();
        return data;
    }
}
