import BasePoller from '../poller';
import Models from '../../models';
const { QueryTypes } = require('sequelize');

/**
 * A function to identify missing stretches of data and create fetch requests for them.
 *
 * Possible Algorithms
 * - Fetch the last week.
 * - Fetch the last month.
 * - Fetch the last year.
 * - Fetch trading days with volume in 2.5 percentile.
 */
export default class AlpacaBarPoller extends BasePoller {

    /**
     * Constructor.
     * @param options
     */
    constructor(options) {
        super(options);
    }

    /**
     * Creates a fetch request for all tweets, one request per 100 tweets.
     * @returns {Promise<EventBridgeMessage>}
     */
    async createFetchRequests(event, context) {
        this.logger.info('AlpacaBarPoller creating fetch requests for event.', event);

        // Query all symbols we are tracking.
        const rows = await Models.sequelize.query("SELECT s.symbol, MIN(sb.measured_at) AS earliest FROM `securities` s LEFT JOIN `bars` sb ON sb.symbol = sb.symbol GROUP BY s.symbol", { type: QueryTypes.SELECT });

        // Create a new message.
        const message = this.eventBridge.createMessage();

        // Iterate over all symbols.
        rows.forEach((row) => {
            let end = (row.earliest == null) ? new Date() : new Date(row.earliest);
            let start = new Date(end.getTime() - 1000 * 3600 * 24 * 7);

            // Add an entry covering a two week stretch.
            let detail = {
                symbol: row.symbol,
                start: start.toISOString().split('T')[0],
                end: end.toISOString().split('T')[0]
            };

            this.logger.info(detail);
            message.addEntry('Bar.Fetch', detail);
        });
        this.logger.info(`AlpacaBarPoller created ${message.getEntries().length} fetch requests for event.`);
        return message;
    }

    /**
     * Creates a fetch request for all tweets, one request per 100 tweets.
     * @returns {Promise<EventBridgeMessage>}
     */
    async createFetchRequestsOld(event, context) {
        this.logger.info('AlpacaBarPoller creating fetch requests for event.', event);

        // Query all symbols we are tracking.
        const symbols = await Models.Symbol.findAll();

        // Create a new message.
        const message = this.eventBridge.createMessage();

        // Iterate over all symbols.
        symbols.forEach((symbol) => {
            let start = new Date(event.detail.start);
            const final = new Date(event.detail.end);
            while (start < final) {
                // Set end to two weeks later.
                let end = new Date(start.getTime() + 1000 * 3600 * 24 * 14);

                // Add an entry covering a two week stretch.
                let detail = {
                    symbol: symbol.symbol,
                    start: start.toISOString().split('T')[0],
                    end: end.toISOString().split('T')[0]
                }
                this.logger.info(detail);
                message.addEntry('Bar.Fetch', detail);

                start = end;
            }
        });
        this.logger.info(`AlpacaBarPoller created ${message.getEntries().length} fetch requests for event.`);
        return message;
    }


}
