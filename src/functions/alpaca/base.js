import BaseFunction from '../base';
import Models from '../../models';
import AlphaVantageManager from '../../services/alpha_vantage_manager';

/**
 * A function to respond to EventBridge fetch requests by fetching specified tweets and saving them.
 *
 * The Twitter TOS requires us to remove deleted/blocked tweets within a reasonable time frame.
 * This helps ensure that happens by fetching updated versions of tweets.
 */
export default class AlpacaBase extends BaseFunction {

    /** @type {AlphaVantageManager} */
    client;

    /**
     * Constructor.
     * @param options
     */
    constructor(options) {
        super(options);
        this.client = options.client;
    }

}
