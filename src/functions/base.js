import Models from "../models";

/**
 * A base function class that other Lambda functions can extend.
 * This helps provide standardized mechanisms for logging and validation.
 * To extend, implement "validate" and "action".
 */
export default class BaseFunction {

    /** @type {Logger} */
    logger;

    /**
     * Creates a base function class.
     * @param options
     */
    constructor(options) {
        this.logger = options.logger;
    }

    /**
     * Handle a lambda event.
     * @param event
     */
    async handle(event, context) {
        this.logger.debug("Entering handler");
        let result = {};
        const errors = this.validate(event);
        if (errors && (errors.length == 0)) {
            this.logger.debug("Passed validation");
            const result =  await this.action(event);
            this.logger.debug("Completed processing");
        }
        else {
            return errors;
        }
        return result;
    }

    /**
     * Validate an event, returning an array of errors messages.
     * @param event
     * @returns {Array}
     */
    validate(event) {
        this.logger.debug("WARNING: Base validator called.");
        return [];
    }

    /**
     * Perform primary action.
     * @param event
     * @returns {Promise<{action: string}>}
     */
    async action(event) {
        this.logger.debug("WARNING: Base action called.");
        return {
            action: "Not yet implemented."
        };
    }
}
