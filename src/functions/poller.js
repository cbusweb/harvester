import BaseFunction from './base';
import EventBridgeManager from '../services/eventbridge/manager';

/**
 * A base Poller function that other polling jobs can extend.
 * Pollers exist to tell a Fetcher to fetch some set(s) of data.
 *
 * Separating the decision of what to fetch from the act of fetching accomplishes two things.
 *
 * 1. It's common for a single logical data sync to involve many requests.
 *    Updating all tweets for an account could involve 20-30 requests to Twitter's API.
 *    Performing each request in a separate "fetcher" lambda helps keep Lambda execution
 *    efficient (fast, low memory) and it also makes it easier to reason about API calls
 *    and rate limits since throttling can happen at the lambda function level.
 *
 * 2. One can't schedule events on any event bus other than the account's default.
 *    It's nice to minimize what happens on the default event bus/bridge and so these polling
 *    job's queue fetches on the application specific event bus/bridge.
 *
 * To implement a poller, you may only need to implement createFetchRequests().
 */
export default class BasePoller extends BaseFunction {

    /** @type {EventBridgeManager} */
    eventBridge;

    /**
     * Creates a base function class.
     * @param options
     */
    constructor(options) {
        super(options);
        this.eventBridge = options.eventBridge;
    }

    /**
     * To implement a Poller, override this, returning an event bridge message.
     * @returns {Promise<Array>}
     */
    async createFetchRequests() {
        this.logger.debug("WARNING: Base createFetchRequests called.");
        return this.eventBridge.createMessage('Unknown', {});
    }

    /**
     * Perform the primary action.
     */
    async action(event, context) {
        this.logger.info('BasePoller initiating action on event.', event);
        const message = await this.createFetchRequests(event, context);
        return this.eventBridge.send(message);
    }

}
