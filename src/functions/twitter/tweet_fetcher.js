import BaseFunction from '../base';
import Models from '../../models';
const Twitter = require('twitter-v2');

/**
 * A function to respond to EventBridge fetch requests by fetching specified tweets and saving them.
 *
 * The Twitter TOS requires us to remove deleted/blocked tweets within a reasonable time frame.
 * This helps ensure that happens by fetching updated versions of tweets.
 */
export default class TweetFetcher extends BaseFunction {

    /** @type {Twitter} */
    client;

    /**
     * Constructor.
     * @param options
     */
    constructor(options) {
        super(options);
        this.client = options.client;
    }

    async action(event, context) {

        const results = {
            adds: 0,
            updates: 0,
            unchanged: 0,
            errors: 0,
            total: 0,
        };

        const { data } = await this.client.get('tweets', { ids: event.detail.ids });
        for (const key in data) {
            let itemRecord = this.itemize(data[key]);
            let item = await Models.Item.findOne({ where: { source_identifier: itemRecord.source_identifier } });
            item.text = itemRecord.text;
            if (item.changed()) {
                item.save();
                results.updates++;
            }
            else {
                results.unchanged++;
            }
            results.total++;
        }
        this.logger.info('Finished processing tweets', results);
    }

    /**
     * Normalize a tweet to Media Magnet's format.
     * @param tweet
     */
    itemize(tweet) {
        return {
            source_identifier: tweet.id,
            description: tweet.text,
        }
    }


}
