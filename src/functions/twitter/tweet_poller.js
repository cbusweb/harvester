import BasePoller from '../poller';
import Models from '../../models';

/**
 * A class to create fetch requests for existing Tweets.
 *
 * The Twitter TOS requires us to remove deleted/blocked tweets within a reasonable time frame.
 * This helps ensure that happens by queueing fetches of existing tweets based on id.
 */
export default class TweetPoller extends BasePoller {

    /**
     * Constructor.
     * @param options
     */
    constructor(options) {
        super(options);
    }

    /**
     * Creates a fetch request for all tweets, one request per 100 tweets.
     * @returns {Promise<EventBridgeMessage>}
     */
    async createFetchRequests() {
        // This is not the right query. Only here for proof of concept.
        const tweets = await Models.Item.findAll();
        const tweetIds = tweets.map(function (tweet) { return tweet.source_identifier });

        // Create a new message.
        const message = this.eventBridge.createMessage();

        // The Twitter v2 endpoint can only handle 100 tweet ids at a time.
        while (tweetIds.length > 0) {
            message.addEntry('Twitter.Tweet.Fetch', {
                ids: tweetIds.splice(0, 100)
            })
        }
        return message;
    }


}
