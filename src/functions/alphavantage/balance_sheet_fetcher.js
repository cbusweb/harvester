import AlphaVantageReportFetcher from './report_fetcher';
import Models from '../../models';

/**
 * Fetch historic prices for a stock.
 */
export default class AlphaVantageBalanceSheetFetcher extends AlphaVantageReportFetcher {

    async getData(symbol, status) {
        const cacheKey = this.cacheKey(status, 'balance_sheets', symbol);

        let data = await this.cache.get(cacheKey);
        if (data) {
            this.logger.debug('Loading ' + data.length + ' records from cache.');
        }
        else {
            this.logger.debug('Cache miss for ' + symbol + ', fetch from API.');
            data = await this.client.balance(symbol);
            this.logger.debug('Received ' + data.length + ' records from API.');
            await this.cache.set(cacheKey, data);
        }
        return data;
    }

    async findOrCreateQuarterly(symbol, fiscalDateEnding, report) {
        return await Models.QuarterlyBalanceSheet.findOrCreate({
            where: {
                symbol: symbol,
                fiscal_date_ending: fiscalDateEnding
            },
            defaults: report
        });
    }

    async findOrCreateAnnual(symbol, fiscalDateEnding, report) {
        return await Models.AnnualBalanceSheet.findOrCreate({
            where: {
                symbol: symbol,
                fiscal_date_ending: fiscalDateEnding
            },
            defaults: report
        });
    }

    map(symbol, report) {
        const mapped = {
            symbol: symbol,
            fiscal_date_ending: new Date(report.fiscalDateEnding),
            reported_currency: report.reportedCurrency,
            total_assets: report.totalAssets,
            total_current_assets: report.totalCurrentAssets,
            cash_and_cash_equivalents_at_carrying_value: report.cashAndCashEquivalentsAtCarryingValue,
            cash_and_short_term_investments: report.cashAndShortTermInvestments,
            inventory: report.inventory,
            current_net_receivables: report.currentNetReceivables,
            total_non_current_assets: report.totalNonCurrentAssets,
            property_plant_equipment:report.propertyPlantEquipment,
            accumulated_depreciation_amortization_ppe: report.accumulatedDepreciationAmortizationPPE,
            intangible_assets: report.intangibleAssets,
            intangible_assets_excluding_goodwill: report.intangibleAssetsExcludingGoodwill,
            goodwill: report.goodwill,
            investments: report.investments,
            long_term_investments: report.longTermInvestments,
            short_term_investments: report.shortTermInvestments,
            other_current_assets: report.otherCurrentAssets,
            other_non_currrent_assets: report.otherNonCurrrentAssets,
            total_liabilities: report.totalLiabilities,
            total_current_liabilities: report.totalCurrentLiabilities,
            current_accounts_payable: report.currentAccountsPayable,
            deferred_revenue: report.deferredRevenue,
            current_debt: report.currentDebt,
            short_term_debt: report.shortTermDebt,
            total_non_current_liabilities: report.totalNonCurrentLiabilities,
            capital_lease_obligations: report.capitalLeaseObligations,
            long_term_debt: report.longTermDebt,
            current_long_term_debt: report.currentLongTermDebt,
            long_term_debt_non_current: report.longTermDebtNoncurrent,
            short_long_term_debt_total: report.shortLongTermDebtTotal,
            other_current_liabilities: report.otherCurrentLiabilities,
            other_non_current_liabilities: report.otherNonCurrentLiabilities,
            total_shareholder_equity: report.totalShareholderEquity,
            treasury_stock: report.treasuryStock,
            retained_earnings: report.retainedEarnings,
            common_stock: report.commonStock,
            common_stock_shares_outstanding: report.commonStockSharesOutstanding
        };

        // AlphaVantage has a number of numerical values marked as None.
        for (const [key, value] of Object.entries(mapped)) {
            if (mapped[key] == 'None') {
                mapped[key] = 0;
            }
        }
        return mapped;
    }
}

/*

 */