import AlphaVantageBase from './base';
import Models from '../../models';

/**
 * Fetch historic prices for a stock.
 */
export default class AlphaVantageItemFetcher extends AlphaVantageBase {

    /** @type {AlphaVantageManager} */
    client;

    /**
     * Constructor.
     * @param options
     */
    constructor(options) {
        super(options);
        this.client = options.client;
    }

    async getData() {
        this.logger.info("Override AlphaVantageItemFetcher.getData().");
    }

    async findOrCreate(symbol, item) {
        this.logger.info("Override AlphaVantageItemFetcher.findOrCreate().");
    }

    map(symbol, data) {
        this.logger.info("Override AlphaVantageItemFetcher.map().");
    }

    async action(event, context) {
        const data = await this.getData();
        await this.update(data);
    }


    async update(apiItems) {
        for (let i=0; i<apiItems.length; i++) {

            let apiItem = this.map(apiItems[i].symbol, apiItems[i]);

            let [item, created] = await this.findOrCreate(apiItems[i].symbol, apiItem);

            if (!created) {
                for (const [key, value] of Object.entries(apiItem)) {
                    item[key] = value;
                }
                item.save();
            }
        }
    }

}
