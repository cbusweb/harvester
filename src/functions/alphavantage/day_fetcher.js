import AlphaVantageSeriesFetcher from './series_fetcher';
import Models from '../../models';

/**
 * Fetch historic prices for a stock.
 */
export default class AlphaVantageDayFetcher extends AlphaVantageSeriesFetcher {

    async getData(symbol, status) {
        const cacheKey = this.cacheKey(status, 'days', symbol);

        let data = await this.cache.get(cacheKey);
        if (data) {
            this.logger.debug('Loading ' + data.length + ' records from cache.');
        }
        else {
            this.logger.debug('Cache miss for ' + symbol + ', fetch from API.');
            data = await this.client.days(symbol);
            this.logger.debug('Received ' + data.length + ' records from API.');
            await this.cache.set(cacheKey, data);
        }
        return data;
    }

    async findOrCreate(symbol, measuredAt, item) {
        return await Models.Day.findOrCreate({
            where: {
                symbol: symbol,
                measured_at: measuredAt
            },
            defaults: item
        });
    }

    map(symbol, item) {
        return {
            symbol: symbol,
            measured_at: new Date(item.timestamp),
            open: item.open,
            high: item.high,
            low: item.low,
            close: item.close,
            volume: item.volume,
            adjusted_close: item.adjusted_close,
            dividend: item.dividend_amount,
            split: item.split_coefficient,
        };
    }
}
