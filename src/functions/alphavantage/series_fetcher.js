import AlphaVantageBase from './base';
import Models from '../../models';

/**
 * Fetch historic prices for a stock.
 */
export default class AlphaVantageSeriesFetcher extends AlphaVantageBase {

    /** @type {AlphaVantageManager} */
    client;

    /**
     * Constructor.
     * @param options
     */
    constructor(options) {
        super(options);
        this.client = options.client;
    }

    async getData(symbol) {
        this.logger.info("Override AlphaVantageReport.getData().");
    }

    async findOrCreate(symbol, fiscalDateEnding) {
        this.logger.info("Override AlphaVantageSeries.findOrCreate().");
    }

    map(symbol, apiReport) {
        this.logger.info("Override AlphaVantageReport.map().");
    }

    async action(event, context) {
        const data = await this.getData(event.detail.symbol, event.detail.status);
        await this.update(event.detail.symbol, data);
    }


    async update(symbol, apiItems) {
        for (let i=0; i<apiItems.length; i++) {

            let apiItem = this.map(symbol, apiItems[i]);

            let [item, created] = await this.findOrCreate(symbol, apiItem.measured_at, apiItem);

            if (!created) {
                for (const [key, value] of Object.entries(apiItem)) {
                    item[key] = value;
                }
                await item.save();
            }
        }
    }

}
