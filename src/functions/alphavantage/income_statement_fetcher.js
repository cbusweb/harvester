import AlphaVantageBase from './base';
import Models from '../../models';

/**
 * Fetch historic prices for a stock.
 */
export default class AlphaVantageIncomeStatementFetcher extends AlphaVantageBase {

    /** @type {AlphaVantageManager} */
    client;

    /**
     * Constructor.
     * @param options
     */
    constructor(options) {
        super(options);
        this.client = options.client;
    }

    async action(event, context) {
        const cacheKey = this.cacheKey(event.detail.status, 'earnings', event.detail.symbol);
        let data = await this.cache.get(cacheKey);
        if (data) {
            this.logger.debug('Loading ' + data.length + ' records from cache.');
        }
        else {
            this.logger.debug('Cache miss for ' + event.detail.symbol + ', fetch from API.');
            data = await this.client.income(event.detail.symbol);
            this.logger.debug('Received ' + data.length + ' records from API.');
            await this.cache.set(cacheKey, data);
        }

        if (data.annualReports) {
            await this.updateAnnual(event.detail.symbol, data.annualReports);
        }
        if (data.quarterlyReports) {
            await this.updateQuarterly(event.detail.symbol, data.quarterlyReports);
        }

    }


    async updateQuarterly(symbol, apiReports) {
        for (let i=0; i<apiReports.length; i++) {
            let apiReport = this.map(symbol, apiReports[i]);
            let [report, created] = await Models.QuarterlyIncomeStatement.findOrCreate({
                where: {
                    symbol: symbol,
                    fiscal_date_ending: apiReport.fiscal_date_ending
                },
                defaults: apiReport
            });

            if (!created) {
                for (const [key, value] of Object.entries(apiReport)) {
                    report[key] = value;
                }
                report.save();
            }
        }
    }

    async updateAnnual(symbol, apiReports) {
        for (let i=0; i<apiReports.length; i++) {
            let apiReport = this.map(symbol, apiReports[i]);
            let [report, created] = await Models.AnnualIncomeStatement.findOrCreate({
                where: {
                    symbol: symbol,
                    fiscal_date_ending: apiReport.fiscal_date_ending
                },
                defaults: apiReport
            });

            if (!created) {
                for (const [key, value] of Object.entries(apiReport)) {
                    report[key] = value;
                }
                report.save();
            }
        }
    }

    map(symbol, apiReport) {
        const mapped = {
            symbol: symbol,
            fiscal_date_ending: new Date(apiReport.fiscalDateEnding),
            reported_currency: apiReport.reportedCurrency,
            gross_profit: apiReport.grossProfit,
            total_revenue: apiReport.totalRevenue,
            cost_of_revenue: apiReport.costOfRevenue,
            // There is an actual typo in the API with of lowercased.
            cost_of_goods_and_services_sold: apiReport.costofGoodsAndServicesSold,
            operating_income: apiReport.operatingIncome,
            selling_general_and_administrative: apiReport.sellingGeneralAndAdministrative,
            research_and_development:  apiReport.researchAndDevelopment,
            operating_expenses: apiReport.operatingExpenses,
            investment_income_net: apiReport.investmentIncomeNet,
            net_interest_income: apiReport.netInterestIncome,
            interest_income: apiReport.interestIncome,
            interest_expense: apiReport.interestExpense,
            non_interest_income: apiReport.nonInterestIncome,
            other_non_operating_income: apiReport.otherNonOperatingIncome,
            depreciation: apiReport.depreciation,
            depreciation_and_amortization: apiReport.depreciationAndAmortization,
            income_before_tax: apiReport.incomeBeforeTax,
            income_tax_expense: apiReport.incomeTaxExpense,
            interest_and_debt_expense: apiReport.interestAndDebtExpense,
            net_income_from_continuing_operations: apiReport.netIncomeFromContinuingOperations,
            comprehensive_income_net_of_tax: apiReport.comprehensiveIncomeNetOfTax,
            ebit: apiReport.ebit,
            ebitda: apiReport.ebitda,
            net_income: apiReport.netIncome,
        }

        // AlphaVantage has a number of numerical values marked as None.
        for (const [key, value] of Object.entries(mapped)) {
            if (mapped[key] == 'None') {
                mapped[key] = 0;
            }
        }
        return mapped;
    }
}
