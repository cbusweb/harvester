import AlphaVantageBase from './base';
import Models from '../../models';

/**
 * Fetch historic prices for a stock.
 */
export default class AlphaVantageReportFetcher extends AlphaVantageBase {

    /** @type {AlphaVantageManager} */
    client;

    /**
     * Constructor.
     * @param options
     */
    constructor(options) {
        super(options);
        this.client = options.client;
    }

    async getData(symbol, status) {
        this.logger.info("Override AlphaVantageReport.getData().");
    }

    async findOrCreateQuarterly(symbol, fiscalDateEnding) {
        this.logger.info("Override AlphaVantageReport.findOrCreateQuarterly().");
    }

    async findOrCreateAnnual(symbol, fiscalDateEnding) {
        this.logger.info("Override AlphaVantageReport.findOrCreateQuarterly().");
    }

    mapQuarterly(symbol, apiReport) {
        return this.map(symbol, apiReport);
    }

    mapAnnual(symbol, apiReport) {
        return this.map(symbol, apiReport);
    }

    map(symbol, apiReport) {
        this.logger.info("Override AlphaVantageReport.map[Annual|Quarterly]().");
    }

    async action(event, context) {
        const data = await this.getData(event.detail.symbol, event.detail.status);
        if (data.annualReports) {
            await this.updateReports('annual', event.detail.symbol, data.annualReports);
        }
        if (data.quarterlyReports) {
            await this.updateReports('quarterly', event.detail.symbol, data.quarterlyReports);
        }
    }


    async updateReports(period, symbol, apiReports) {
        for (let i=0; i<apiReports.length; i++) {

            let apiReport = (period == 'annual')
                ? this.mapAnnual(symbol, apiReports[i])
                : this.mapQuarterly(symbol, apiReports[i]);

            let [report, created] = (period == 'annual')
                ?  await this.findOrCreateAnnual(symbol, apiReport.fiscal_date_ending, apiReport)
                :  await this.findOrCreateQuarterly(symbol, apiReport.fiscal_date_ending, apiReport);

            if (!created) {
                for (const [key, value] of Object.entries(apiReport)) {
                    report[key] = value;
                }
                report.save();
            }
        }
    }

}
