import BaseFunction from '../base';
import Models from '../../models';
import AlphaVantageManager from '../../services/alpha_vantage_manager';
import CacheManager from '../../services/cache_manager';

/**
 * A function to respond to EventBridge fetch requests by fetching specified tweets and saving them.
 *
 * The Twitter TOS requires us to remove deleted/blocked tweets within a reasonable time frame.
 * This helps ensure that happens by fetching updated versions of tweets.
 */
export default class AlphaVantageBase extends BaseFunction {

    /** @type {AlphaVantageManager} */
    client;

    /** @type { CacheManager } */
    cache;

    /**
     * Constructor.
     * @param options
     */
    constructor(options) {
        super(options);
        this.cache = options.cache;
        this.client = options.client;
    }

    cacheKey(status, prefix, symbol) {
        let cacheKey =  status + '/' + prefix + '/' + symbol.substring(0, 1) + '/';
        if (symbol.length > 1) {
            cacheKey += symbol.substring(1, 2) + '/';
        }
        cacheKey += symbol;
        return cacheKey.toLowerCase();
    }

}
