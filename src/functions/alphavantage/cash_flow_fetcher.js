import AlphaVantageReportFetcher from './report_fetcher';
import Models from '../../models';

/**
 * Fetch historic prices for a stock.
 */
export default class AlphaVantageCashFlowFetcher extends AlphaVantageReportFetcher {

    async getData(symbol, status) {
        const cacheKey = this.cacheKey(status, 'cash_flows', symbol);

        let data = await this.cache.get(cacheKey);
        if (data) {
            this.logger.debug('Loading ' + data.length + ' records from cache.');
        }
        else {
            this.logger.debug('Cache miss for ' + symbol + ', fetch from API.');
            data = await this.client.cashFlow(symbol);
            this.logger.debug('Received ' + data.length + ' records from API.');
            await this.cache.set(cacheKey, data);
        }
        return data;
    }

    async findOrCreateQuarterly(symbol, fiscalDateEnding, report) {
        return await Models.QuarterlyCashFlow.findOrCreate({
            where: {
                symbol: symbol,
                fiscal_date_ending: fiscalDateEnding
            },
            defaults: report
        });
    }

    async findOrCreateAnnual(symbol, fiscalDateEnding, report) {
        console.log({symbol: symbol, fiscalDateEnding: fiscalDateEnding, report: report});
        return await Models.AnnualCashFlow.findOrCreate({
            where: {
                symbol: symbol,
                fiscal_date_ending: fiscalDateEnding
            },
            defaults: report
        });
    }

    map(symbol, report) {
        const mapped = {
            symbol: symbol,
            fiscal_date_ending: new Date(report.fiscalDateEnding),
            reported_currency: report.reportedCurrency,
        };

        const exceptions = {
            proceedsFromIssuanceOfLongTermDebtAndCapitalSecuritiesNet: 'proceeds_from_issuance_of_lt_debt_and_cap_securities_net'
        };

        // AlphaVantage has a number of numerical values marked as None.
        for (const [key, value] of Object.entries(report)) {
            let field = key.split(/(?=[A-Z])/).join('_').toLowerCase();
            if (exceptions.hasOwnProperty(key)) {
                field = exceptions[key];
            }
            mapped[field] = (report[key] == 'None') ? 0 : report[key];
        }
        return mapped;
    }
}

/*

 */