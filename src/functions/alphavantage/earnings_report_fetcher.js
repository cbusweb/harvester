import AlphaVantageBase from './base';
import Models from '../../models';

/**
 * Fetch historic prices for a stock.
 */
export default class AlphaVantageEarningsReportFetcher extends AlphaVantageBase {

    /** @type {AlphaVantageManager} */
    client;

    /**
     * Constructor.
     * @param options
     */
    constructor(options) {
        super(options);
        this.client = options.client;
    }

    async action(event, context) {
        const cacheKey = this.cacheKey(event.detail.status, 'earnings', event.detail.symbol);
        let data = await this.cache.get(cacheKey);
        if (data) {
            this.logger.debug('Loading ' + data.length + ' records from cache.');
        }
        else {
            this.logger.debug('Cache miss for ' + event.detail.symbol + ', fetch from API.');
            data = await this.client.earnings(event.detail.symbol);
            this.logger.debug('Received ' + data.length + ' records from API.');
            await this.cache.set(cacheKey, data);
        }

        if (data.annualEarnings) {
            await this.updateAnnual(event.detail.symbol, data.annualEarnings);
        }
        if (data.quarterlyEarnings) {
            await this.updateQuarterly(event.detail.symbol, data.quarterlyEarnings);
        }
    }

    async updateQuarterly(symbol, apiReports) {
        for (let i=0; i<apiReports.length; i++) {
            let apiReport = this.mapQuarterly(symbol, apiReports[i]);
            let [report, created] = await Models.QuarterlyEarningsReport.findOrCreate({
                where: {
                    symbol: symbol,
                    fiscal_date_ending: apiReport.fiscal_date_ending
                },
                defaults: apiReport
            });

            if (!created) {
                report.reported_date = apiReport.reported_date;
                report.reported_eps = apiReport.reported_eps;
                report.estimated_eps = apiReport.estimated_eps;
                report.surprise = apiReport.surprise;
                report.surprise_percentage = apiReport.surprise_percentage;
                report.save();
            }
        }
    }

    mapQuarterly(symbol, apiReport) {
        return {
            symbol: symbol,
            fiscal_date_ending: new Date(apiReport.fiscalDateEnding),
            reported_date: new Date(apiReport.reportedDate),
            reported_eps: isNaN(apiReport.reportedEPS) ? null : apiReport.reportedEPS,
            estimated_eps: isNaN(apiReport.estimatedEPS) ? null : apiReport.estimatedEPS,
            surprise: isNaN(apiReport.surprise) ? null : apiReport.surprise,
            surprise_percentage: isNaN(apiReport.surprisePercentage) ? null : apiReport.surprise
        }
    }

    async updateAnnual(symbol, apiReports) {
        for (let i=0; i<apiReports.length; i++) {
            let apiReport = this.mapAnnual(symbol, apiReports[i]);
            let [report, created] = await Models.AnnualEarningsReport.findOrCreate({
                where: {
                    symbol: symbol,
                    fiscal_date_ending: apiReport.fiscal_date_ending
                },
                defaults: apiReport
            });

            if (!created) {
                report.reported_eps = apiReport.reported_eps;
                report.save();
            }
        }
    }

    mapAnnual(symbol, apiReport) {
        return {
            symbol: symbol,
            fiscal_date_ending: new Date(apiReport.fiscalDateEnding),
            reported_eps: isNaN(apiReport.reportedEPS) ? null : apiReport.reportedEPS
        }
    }
}
