import AlphaVantageBase from './base';
import Models from '../../models';

/**
 * Fetch historic prices for a stock.
 */
export default class AlphaVantageHistoryPriceFetcher extends AlphaVantageBase {

    /** @type {AlphaVantageManager} */
    client;

    /**
     * Constructor.
     * @param options
     */
    constructor(options) {
        super(options);
        this.client = options.client;
    }

    async action(event, context) {
        const data = await this.client.intraday(event.detail.symbol, '60min');
        this.logger.info(data);
        this.logger.info(event);
    }

    /*
    async action(event, context) {

        const results = {
            adds: 0,
            updates: 0,
            unchanged: 0,
            errors: 0,
            total: 0,
        };

        const { data } = await this.client.get('tweets', { ids: event.detail.ids });
        for (const key in data) {
            let itemRecord = this.itemize(data[key]);
            let item = await Models.Item.findOne({ where: { source_identifier: itemRecord.source_identifier } });
            item.text = itemRecord.text;
            if (item.changed()) {
                item.save();
                results.updates++;
            }
            else {
                results.unchanged++;
            }
            results.total++;
        }
        this.logger.info('Finished processing tweets', results);
    }
    */

}
