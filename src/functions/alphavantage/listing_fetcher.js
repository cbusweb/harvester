import AlphaVantageItemFetcher from './item_fetcher';
import Models from '../../models';

/**
 * Fetch security listings.
 */
export default class AlphaVantageListingFetcher extends AlphaVantageItemFetcher {

    async getData() {
        let data = await this.client.listings();
        return data;
    }

    async findOrCreate(symbol, item) {
        return await Models.Security.findOrCreate({
            where: {
                symbol: symbol
            },
            defaults: item
        });
    }

    map(symbol, item) {
        return {
            symbol: symbol,
            name: item.name,
            exchange: item.exchange,
            type: item.assetType,
            ipo_date: item.ipoDate.length > 5 ? new Date(item.ipoDate) : null,
            delisting_date: item.delistingDate.length > 5 ? new Date(item.delistingDate): null,
            status: item.status,
        };
    }
}
