import PolygonBase from './base';
import Models from '../../models';

/**
 * Fetch dividends for a stock.
 */
export default class PolygonDividendFetcher extends PolygonBase {

    /** @type {PolygonManager} */
    client;

    /**
     * Constructor.
     * @param options
     */
    constructor(options) {
        super(options);
        this.client = options.client;
    }

    async action(event, context) {
        this.logger.info('AlpacaBarFetcher initiating action on event.', event);
        const symbol = event.detail.symbol;

        let resp = this.client.getBarsV2(
            symbol,
            {
                start: event.detail.start,
                end: event.detail.end,
                limit: 10000,
                timeframe: '1Min',
            },
            this.client.configuration
        );

        // We will need to sort and manipulate the resultset, so first transform it into a list of bars.
        let bars = [];
        for await (let item of resp) {
            bars.push(this.mapBar(symbol, item));
        }

        // Sort the bars in chronological order.
        bars = bars.sort((a, b) => a.measured_at > b.measured_at);

        // Augment bars with information about runs.
        let closeLastAt, closeRunLength, closeRunVolume, closeRunStart, closeRunLast;
        for (let i=0; i<bars.length; i++) {

            if (this.continueRun(closeRunStart, closeRunLast, bars[i].close, closeLastAt, bars[i].measured_at)) {
                closeRunLength++;
                closeRunVolume += bars[i].volume;
                closeRunLast = bars[i].close;
            }
            else {
                closeRunLength = 0;
                closeRunVolume = bars[i].volume;
                closeRunStart = bars[i].open;
                closeRunLast = bars[i].close;
            }

            // Update our bar.
            bars[i].close_run_change = bars[i].close / closeRunStart;
            bars[i].close_run_length = closeRunLength;
            bars[i].close_run_volume = closeRunVolume;

            // Look
            if (i + 1 == bars.length) {
                bars[i].close_run_inactivity_stop = true;
            }
            else {
                bars[i].close_run_inactivity_stop = this.runTerminatedForInactivity(bars[i].measured_at, bars[i+1].measured_at)
            }

            closeLastAt = bars[i].measured_at;
        }

        let resultCount = 0;
        for (let bar of bars) {
            let [symbolBar, created] = await Models.SymbolBar.findOrCreate({
                where: {
                    symbol: symbol,
                    measured_at: bar.measured_at
                },
                defaults: bar
            });
            if (created) {
                this.logger.info(`${symbolBar.symbol} created for ${symbolBar.measured_at}`);
            }
            resultCount++;
        }
        this.logger.info(`AlpacaBarFetcher retrieved ${resultCount} bars.`)
    }


    mapBar(symbol, alpacaBar) {
        return {
            symbol: symbol,
            measured_at: new Date(alpacaBar.Timestamp),
            open: alpacaBar.OpenPrice,
            high: alpacaBar.HighPrice,
            low: alpacaBar.LowPrice,
            close: alpacaBar.ClosePrice,
            volume: alpacaBar.Volume
        }
    }

    continueRun(start, last, current, closeLastAt, closeAt) {
        // Null values indicate a run has been interrupted.
        const hasNulls = (start == null) || (last == null) || (last == current) || (closeLastAt == null) || (closeAt == null) ;
        if (hasNulls) {
            return false;
        }

        // Readings should be 60 seconds apart ton continue a run.
        const correctStepSize = closeAt.getTime() - closeLastAt.getTime() == 60000;

        // Run direction must remain consistent for it to continue
        const stillUp = (current > last) && (last > start);
        const stillDown = (current < last) && (last < start);

        return correctStepSize && (stillUp || stillDown);
    }

    runTerminatedForInactivity(currentTime, nextTime) {
        return nextTime.getTime() - currentTime.getTime() != 60000;
    }
}
