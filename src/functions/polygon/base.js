import BaseFunction from '../base';
import Models from '../../models';
import PolygonManager from '../../services/polygon_manager';


export default class PolygonBase extends BaseFunction {

    /** @type {PolygonManager} */
    client;

    /**
     * Constructor.
     * @param options
     */
    constructor(options) {
        super(options);
        this.client = options.client;
    }

}
