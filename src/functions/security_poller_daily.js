import BasePoller from './poller';
import Models from '../models';
const { QueryTypes } = require('sequelize');

export default class SecurityPollerDaily extends BasePoller {

    /**
     * To implement a Poller, override this, returning an event bridge message.
     * @returns {Promise<Array>}
     */
    async createFetchRequests(event, context) {
        this.logger.info('DailySecurityPoller creating fetch requests for event.', event);

        // Query all symbols we are tracking.
        //const rows = await Models.sequelize.query("SELECT symbol FROM securities WHERE status = 'Active' ORDER BY CASE WHEN last_daily_poll is null then 0 else 1 end, last_daily_poll LIMIT 5 ", { type: QueryTypes.SELECT });
        const securities = await Models.Security.findAll({
            /*
            where: {
                status: 'Active',
            },
            */
            order: [
                // Will escape title and validate DESC against a list of valid direction parameters
                [Models.sequelize.literal(' CASE WHEN last_daily_poll is null then 0 else 1 end'), 'ASC'],
                ['last_daily_poll', 'ASC'],
                ['symbol', 'ASC'],
            ],
            limit: 2
        })
        // Create a new message.
        const message = this.eventBridge.createMessage();

        // Iterate over all symbols.
        securities.forEach((security) => {
            message.addEntry('Balance.Fetch', {
                symbol: security.symbol,
                status: security.status,
            });
            message.addEntry('CashFlow.Fetch', {
                symbol: security.symbol,
                status: security.status,
            });
            message.addEntry('Day.Fetch', {
                symbol: security.symbol,
                status: security.status,
            });
            message.addEntry('Earnings.Fetch', {
                symbol: security.symbol,
                status: security.status,
            });
            message.addEntry('Income.Fetch', {
                symbol: security.symbol,
                status: security.status,
            });

            security.last_daily_poll = new Date();
            security.save();
        });
        this.logger.info(`DailySecurityPoller created ${message.getEntries().length} fetch requests for event.`);
        return message;
    }

}
