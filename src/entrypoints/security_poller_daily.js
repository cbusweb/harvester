/* eslint-disable-next-line no-global-assign */
require = require('esm')(module);
const container = require('../dependencies').default;
const SecurityPollerDaily = require('../functions/security_poller_daily').default;
const lambda = new SecurityPollerDaily({
    eventBridge: container.EventBridgeManager,
    logger: container.Logger,
});

module.exports.handler = function (event, context) {
    return lambda.handle(event, context);
};
