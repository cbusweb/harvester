/* eslint-disable-next-line no-global-assign */
require = require('esm')(module);
const container = require('../../dependencies').default;
const PolygonDividendFetcher = require('../../functions/polygon/dividend_fetcher').default;
const lambda = new PolygonDividendFetcher({
    client: container.PolygonManager,
    logger: container.Logger,
});

module.exports.handler = function (event, context) {
    return lambda.handle(event, context);
};
