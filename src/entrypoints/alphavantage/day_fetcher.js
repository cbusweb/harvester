/* eslint-disable-next-line no-global-assign */
require = require('esm')(module);
const container = require('../../dependencies').default;
const AlphaVantageDayFetcher = require('../../functions/alphavantage/day_fetcher').default;
const lambda = new AlphaVantageDayFetcher({
    cache: container.CacheManager,
    client: container.AlphaVantageManager,
    logger: container.Logger,
});

module.exports.handler = async function (event, context) {
    return await lambda.handle(event, context);
};
