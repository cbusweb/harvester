/* eslint-disable-next-line no-global-assign */
require = require('esm')(module);
const container = require('../../dependencies').default;
const AlphaVantageIncomeStatementFetcher = require('../../functions/alphavantage/income_statement_fetcher').default;
const lambda = new AlphaVantageIncomeStatementFetcher({
    cache: container.CacheManager,
    client: container.AlphaVantageManager,
    logger: container.Logger,
});

module.exports.handler = function (event, context) {
    return lambda.handle(event, context);
};
