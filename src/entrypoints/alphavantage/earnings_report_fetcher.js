/* eslint-disable-next-line no-global-assign */
require = require('esm')(module);
const container = require('../../dependencies').default;
const AlphaVantageEarningsReportFetcher = require('../../functions/alphavantage/earnings_report_fetcher').default;
const lambda = new AlphaVantageEarningsReportFetcher({
    cache: container.CacheManager,
    client: container.AlphaVantageManager,
    logger: container.Logger,
});

module.exports.handler = function (event, context) {
    return lambda.handle(event, context);
};
