/* eslint-disable-next-line no-global-assign */
require = require('esm')(module);
const container = require('../../dependencies').default;
const AlphaVantageBalanceSheetFetcher = require('../../functions/alphavantage/balance_sheet_fetcher').default;
const lambda = new AlphaVantageBalanceSheetFetcher({
    cache: container.CacheManager,
    client: container.AlphaVantageManager,
    logger: container.Logger,
});

module.exports.handler = function (event, context) {
    return lambda.handle(event, context);
};
