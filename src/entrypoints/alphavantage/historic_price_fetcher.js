/* eslint-disable-next-line no-global-assign */
require = require('esm')(module);
const container = require('../../dependencies').default;
const AlphaVantageHistoricPriceFetcher = require('../../functions/alphavantage/historic_price_fetcher').default;
const lambda = new AlphaVantageHistoricPriceFetcher({
    cache: container.CacheManager,
    client: container.AlphaVantageManager,
    logger: container.Logger,
});

module.exports.handler = function (event, context) {
    lambda.handle(event, context);
};
