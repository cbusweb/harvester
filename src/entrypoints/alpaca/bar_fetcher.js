/* eslint-disable-next-line no-global-assign */
require = require('esm')(module);
const container = require('../../dependencies').default;
const AlpacaBarFetcher = require('../../functions/alpaca/bar_fetcher').default;
const lambda = new AlpacaBarFetcher({
    client: container.AlpacaClient,
    logger: container.Logger,
});

module.exports.handler = function (event, context) {
    return lambda.handle(event, context);
};
