/* eslint-disable-next-line no-global-assign */
require = require('esm')(module);
const container = require('../../dependencies').default;
const AlpacaBarPoller = require('../../functions/alpaca/bar_poller').default;
const lambda = new AlpacaBarPoller({
    eventBridge: container.EventBridgeManager,
    logger: container.Logger,
});

module.exports.handler = function (event, context) {
    return lambda.handle(event, context);
};
