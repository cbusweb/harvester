'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class AnnualIncomeStatement extends Model {
    static associate(models) {
      // define association here
    }
  };
  AnnualIncomeStatement.init({
    symbol: {
      type: DataTypes.STRING,
      primaryKey: true,
    },
    fiscal_date_ending: {
      type: DataTypes.DATEONLY,
      primaryKey: true
    },
    reported_currency: DataTypes.STRING(5),
    gross_profit: DataTypes.DECIMAL(20, 2),
    total_revenue: DataTypes.DECIMAL(20, 2),
    cost_of_revenue: DataTypes.DECIMAL(20, 2),
    cost_of_goods_and_services_sold: DataTypes.DECIMAL(20, 2),
    operating_income: DataTypes.DECIMAL(20, 2),
    selling_general_and_administrative: DataTypes.DECIMAL(20, 2),
    research_and_development:  DataTypes.DECIMAL(20, 2),
    operating_expenses: DataTypes.DECIMAL(20, 2),
    investment_income_net: DataTypes.DECIMAL(20, 2),
    net_interest_income: DataTypes.DECIMAL(20, 2),
    interest_income: DataTypes.DECIMAL(20, 2),
    interest_expense: DataTypes.DECIMAL(20, 2),
    non_interest_income: DataTypes.DECIMAL(20, 2),
    other_non_operating_income: DataTypes.DECIMAL(20, 2),
    depreciation: DataTypes.DECIMAL(20, 2),
    depreciation_and_amortization: DataTypes.DECIMAL(20, 2),
    income_before_tax: DataTypes.DECIMAL(20, 2),
    income_tax_expense: DataTypes.DECIMAL(20, 2),
    interest_and_debt_expense: DataTypes.DECIMAL(20, 2),
    net_income_from_continuing_operations: DataTypes.DECIMAL(20, 2),
    comprehensive_income_net_of_tax: DataTypes.DECIMAL(20, 2),
    ebit: DataTypes.DECIMAL(20, 2),
    ebitda: DataTypes.DECIMAL(20, 2),
    net_income: DataTypes.DECIMAL(20, 2),
  }, {
    sequelize,
    modelName: 'AnnualIncomeStatement',
    underscored: true
  });
  return AnnualIncomeStatement;
};



