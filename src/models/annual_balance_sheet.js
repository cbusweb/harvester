'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class AnnualBalanceSheet extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  AnnualBalanceSheet.init({
    symbol: {
      type: DataTypes.STRING,
      primaryKey: true,
    },
    fiscal_date_ending: {
      type: DataTypes.DATEONLY,
      primaryKey: true
    },
    reported_currency: {
      type: DataTypes.STRING(5)
    },
    total_assets: {
      type: DataTypes.DECIMAL(20, 2)
    },
    total_current_assets: {
      type: DataTypes.DECIMAL(20, 2)
    },
    cash_and_cash_equivalents_at_carrying_value: {
      type: DataTypes.DECIMAL(20, 2)
    },
    cash_and_short_term_investments: {
      type: DataTypes.DECIMAL(20, 2)
    },
    inventory: {
      type: DataTypes.DECIMAL(20, 2)
    },
    current_net_receivables: {
      type: DataTypes.DECIMAL(20, 2)
    },
    total_non_current_assets: {
      type: DataTypes.DECIMAL(20, 2)
    },
    property_plant_equipment: {
      type: DataTypes.DECIMAL(20, 2)
    },
    accumulated_depreciation_amortization_ppe: {
      type: DataTypes.DECIMAL(20, 2)
    },
    intangible_assets: {
      type: DataTypes.DECIMAL(20, 2)
    },
    intangible_assets_excluding_goodwill: {
      type: DataTypes.DECIMAL(20, 2)
    },
    goodwill: {
      type: DataTypes.DECIMAL(20, 2)
    },
    investments: {
      type: DataTypes.DECIMAL(20, 2)
    },
    long_term_investments: {
      type: DataTypes.DECIMAL(20, 2)
    },
    short_term_investments: {
      type: DataTypes.DECIMAL(20, 2)
    },
    other_current_assets: {
      type: DataTypes.DECIMAL(20, 2)
    },
    other_non_currrent_assets: {
      type: DataTypes.DECIMAL(20, 2)
    },
    total_liabilities: {
      type: DataTypes.DECIMAL(20, 2)
    },
    total_current_liabilities: {
      type: DataTypes.DECIMAL(20, 2)
    },
    current_accounts_payable: {
      type: DataTypes.DECIMAL(20, 2)
    },
    deferred_revenue: {
      type: DataTypes.DECIMAL(20, 2)
    },
    current_debt: {
      type: DataTypes.DECIMAL(20, 2)
    },
    short_term_debt: {
      type: DataTypes.DECIMAL(20, 2)
    },
    total_non_current_liabilities: {
      type: DataTypes.DECIMAL(20, 2)
    },
    capital_lease_obligations: {
      type: DataTypes.DECIMAL(20, 2)
    },
    long_term_debt: {
      type: DataTypes.DECIMAL(20, 2)
    },
    current_long_term_debt: {
      type: DataTypes.DECIMAL(20, 2)
    },
    long_term_debt_non_current: {
      type: DataTypes.DECIMAL(20, 2)
    },
    short_long_term_debt_total: {
      type: DataTypes.DECIMAL(20, 2)
    },
    other_current_liabilities: {
      type: DataTypes.DECIMAL(20, 2)
    },
    other_non_current_liabilities: {
      type: DataTypes.DECIMAL(20, 2)
    },
    total_shareholder_equity: {
      type: DataTypes.DECIMAL(20, 2)
    },
    treasury_stock: {
      type: DataTypes.DECIMAL(20, 2)
    },
    retained_earnings: {
      type: DataTypes.DECIMAL(20, 2)
    },
    common_stock: {
      type: DataTypes.DECIMAL(20, 2)
    },
    common_stock_shares_outstanding: {
      type: DataTypes.DECIMAL(20, 2)
    }
  }, {
    sequelize,
    modelName: 'AnnualBalanceSheet',
    underscored: true
  });
  return AnnualBalanceSheet;
};


