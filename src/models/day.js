'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Day extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  Day.init({
    symbol: {
      type: DataTypes.STRING,
      primaryKey: true,
    },
    measured_at: {
      type: DataTypes.DATEONLY,
      primaryKey: true,
    },
    open: DataTypes.DECIMAL(20, 2),
    high: DataTypes.DECIMAL(20, 2),
    low: DataTypes.DECIMAL(20, 2),
    close: DataTypes.DECIMAL(20, 2),
    volume: DataTypes.INTEGER,
    adjusted_close: DataTypes.DECIMAL(20, 2),
    dividend: DataTypes.DECIMAL(20, 2),
    split: DataTypes.DECIMAL(20, 2)
  }, {
    sequelize,
    modelName: 'Day',
    tableName: 'days',
    underscored: true
  });
  return Day;
};