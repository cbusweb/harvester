'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Bar extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  Bar.init({
    symbol: {
      type: DataTypes.STRING,
      primaryKey: true,
    },
    measured_at: {
      type: DataTypes.DATE,
      primaryKey: true,
    },
    open: DataTypes.DECIMAL(20, 2),
    high: DataTypes.DECIMAL(20, 2),
    low: DataTypes.DECIMAL(20, 2),
    close: DataTypes.DECIMAL(20, 2),
    volume: DataTypes.INTEGER,
    close_run_length: DataTypes.INTEGER,
    close_run_change: DataTypes.DECIMAL(20, 2),
    close_run_volume: DataTypes.INTEGER,
    close_run_inactivity_stop: DataTypes.BOOLEAN
  }, {
    sequelize,
    modelName: 'Bar',
    tableName: 'bars',
    timestamps: false
  });
  return Bar;
};