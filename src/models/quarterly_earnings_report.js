'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class QuarterlyEarningsReport extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  QuarterlyEarningsReport.init({
    symbol: {
      type: DataTypes.STRING,
      primaryKey: true,
    },
    fiscal_date_ending: {
      type: DataTypes.DATEONLY,
      primaryKey: true
    },
    reported_date: DataTypes.DATE,
    reported_eps: DataTypes.DECIMAL(20, 2),
    estimated_eps: DataTypes.DECIMAL(20, 2),
    surprise: DataTypes.DECIMAL(20, 2),
    surprise_percentage: DataTypes.DECIMAL(20, 2)
  }, {
    sequelize,
    modelName: 'QuarterlyEarningsReport',
    underscored: true
  });
  return QuarterlyEarningsReport;
};


