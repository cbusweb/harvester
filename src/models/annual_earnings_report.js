'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class AnnualEarningsReport extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  AnnualEarningsReport.init({
    symbol: {
      type: DataTypes.STRING,
      primaryKey: true,
    },
    fiscal_date_ending: {
      type: DataTypes.DATEONLY,
      primaryKey: true
    },
    reported_eps: DataTypes.DECIMAL(20, 2),
  }, {
    sequelize,
    modelName: 'AnnualEarningsReport',
    underscored: true
  });
  return AnnualEarningsReport;
};


