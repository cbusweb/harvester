'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Security extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  Security.init({
    symbol: {
      type: DataTypes.STRING,
      primaryKey: true,
    },
    name: DataTypes.STRING,
    exchange: DataTypes.STRING,
    type: DataTypes.STRING,
    ipo_date: DataTypes.DATEONLY,
    delisting_date: DataTypes.DATEONLY,
    status: DataTypes.STRING,
    last_daily_poll: DataTypes.DATE,
  }, {
    sequelize,
    modelName: 'Security',
    underscored: true
  });
  return Security;
};