'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class QuarterlyCashFlow extends Model {
    static associate(models) {
      // define association here
    }
  };
  QuarterlyCashFlow.init({
    symbol: {
      type: DataTypes.STRING,
      primaryKey: true,
    },
    fiscal_date_ending: {
      type: DataTypes.DATEONLY,
      primaryKey: true
    },
    reported_currency: DataTypes.STRING(5),
    operating_cashflow: DataTypes.DECIMAL(20, 2),
    payments_for_operating_activities: DataTypes.DECIMAL(20, 2),
    proceeds_from_operating_activities: DataTypes.DECIMAL(20, 2),
    change_in_operating_liabilities: DataTypes.DECIMAL(20, 2),
    change_in_operating_assets: DataTypes.DECIMAL(20, 2),
    depreciation_depletion_and_amortization: DataTypes.DECIMAL(20, 2),
    capital_expenditures: DataTypes.DECIMAL(20, 2),
    change_in_receivables: DataTypes.DECIMAL(20, 2),
    change_in_inventory: DataTypes.DECIMAL(20, 2),
    profit_loss: DataTypes.DECIMAL(20, 2),
    cashflow_from_investment: DataTypes.DECIMAL(20, 2),
    cashflow_from_financing: DataTypes.DECIMAL(20, 2),
    proceeds_from_repayments_of_short_term_debt: DataTypes.DECIMAL(20, 2),
    payments_for_repurchase_of_common_stock: DataTypes.DECIMAL(20, 2),
    payments_for_repurchase_of_equity: DataTypes.DECIMAL(20, 2),
    payments_for_repurchase_of_preferred_stock: DataTypes.DECIMAL(20, 2),
    dividend_payout: DataTypes.DECIMAL(20, 2),
    dividend_payout_common_stock: DataTypes.DECIMAL(20, 2),
    dividend_payout_preferred_stock: DataTypes.DECIMAL(20, 2),
    proceeds_from_issuance_of_common_stock: DataTypes.DECIMAL(20, 2),
    proceeds_from_issuance_of_lt_debt_and_cap_securities_net: DataTypes.DECIMAL(20, 2),
    proceeds_from_issuance_of_preferred_stock: DataTypes.DECIMAL(20, 2),
    proceeds_from_repurchase_of_equity: DataTypes.DECIMAL(20, 2),
    proceeds_from_sale_of_treasury_stock: DataTypes.DECIMAL(20, 2),
    change_in_cash_and_cash_equivalents: DataTypes.DECIMAL(20, 2),
    change_in_exchange_rate: DataTypes.DECIMAL(20, 2),
    net_income: DataTypes.DECIMAL(20, 2),
  }, {
    sequelize,
    modelName: 'QuarterlyCashFlow',
    underscored: true
  });
  return QuarterlyCashFlow;
};



