module.exports = {
    /**
     * Application configuration section
     * http://pm2.keymetrics.io/docs/usage/application-declaration/
     */
    apps: [{
        name: 'harvester',
        script: 'npm',
        watch: true,
        args: 'run start',
        instances  : 1,
    }],
};