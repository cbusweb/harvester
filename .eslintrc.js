module.exports = {
    root: true,
    extends: '@serverless/eslint-config/node',
    env: {
        es6: true,
        jest: true
    },
    parserOptions: {
        ecmaVersion: 11,
        sourceType: 'module'
    },
    plugins: ['import', 'jest'],
    rules: {
        'object-shorthand': 0,
        'prefer-template': 0,
    }
}
